using System;
using System.Collections.Generic;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using ProjectRefactor.Models;
using ProjectRefactor.Services;

namespace ProjectRefactor
{
    public class AutomaticRegisters
    {
        private ContentService Service { get; }

        public AutomaticRegisters(ContentService service)
        {
            Service = service;
        }


        [FunctionName("AutomaticRegisters")]
        public void Run([TimerTrigger("0 */1 * * * *", RunOnStartup = true)]TimerInfo myTimer, ILogger log)
        {
            var total = 100;

            for(var i=0; i<total; i++)
            {
                var model = new ContentModel
                {
                    Uid = Guid.NewGuid(),
                    Name = $"Index:{i}",
                    Metadata = new Dictionary<string, string>
                    {
                        { "xxxx", Guid.NewGuid().ToString() },
                        { "zzzz", DateTime.Now.ToString() }
                    }
                };

                Service.Add(model);
            }
        }
    }
}
