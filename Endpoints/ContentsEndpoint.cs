using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProjectRefactor.Services;

namespace ProjectRefactor.Endpoints
{
    public class ContentsEndpoint
    {
        private ContentService Service { get; }

        public ContentsEndpoint(ContentService service)
        {
            Service = service;
        }

        [FunctionName("ContentList")]
        public IActionResult ContentList(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "contents")] HttpRequest req,
            ILogger log)
        {
            var models = Service.List();
            return new OkObjectResult(models);
        }


        [FunctionName("ContentCount")]
        public IActionResult ContentCount(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "contents/count")] HttpRequest req,
            ILogger log)
        {
            var models = Service.List();
            return new OkObjectResult(models.Count);
        }


        [FunctionName("ContentByName")]
        public IActionResult ContentByName(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "contents/name/{name}")] HttpRequest req,
            string name,
            ILogger log)
        {
            return new OkObjectResult(new { implementar = $"Servi�o deve retornar os conte�dos com o nome '{name}'" });
        }

        [FunctionName("DeleteContentByName")]
        public IActionResult DeleteContentByName(
            [HttpTrigger(AuthorizationLevel.Function, "Delete", Route = "contents/name/{name}")] HttpRequest req,
            string name,
            ILogger log)
        {
            return new OkObjectResult(new { implementar = $"Servi�o deve remover os conte�dos com o nome '{name}'" });
        }

        [FunctionName("ContentByUid")]
        public IActionResult ContentByUid(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "contents/uid/{uid}")] HttpRequest req,
            Guid uid,
            ILogger log)
        {
            return new OkObjectResult(new { implementar = $"Servi�o deve retornar os conte�dos com base em um id (GUID) �nico '{uid}'" });
        }

        [FunctionName("DeleteContentByUid")]
        public IActionResult DeleteContentByUid(
            [HttpTrigger(AuthorizationLevel.Function, "Delete", Route = "contents/delete/{uid}")] HttpRequest req,
            Guid uid,
            ILogger log)
        {
            return new OkObjectResult(new { implementar = $"Servi�o deve remover os conte�dos com o Uid '{uid}'" });
        }
    }
}

