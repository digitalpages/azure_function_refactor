﻿using ProjectRefactor.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectRefactor.Services
{
    public class ContentService
    {
        private List<ContentModel> Models { get; }

        public ContentService()
        {
            Models = new List<ContentModel>();
        }

        public bool Add(ContentModel model)
        {
            Models.Add(model);
            return true;
        }

        public List<ContentModel> List()
        {
            return Models;
        }

        public bool Remove(ContentModel model)
        {
            var status = Models.Remove(model);
            return status;
        }
    }
}
