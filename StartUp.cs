﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using ProjectRefactor;
using ProjectRefactor.Services;

[assembly: FunctionsStartup(typeof(StartUp))]
namespace ProjectRefactor
{
    public class StartUp : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddSingleton(typeof(ContentService));
        }
    }
}
