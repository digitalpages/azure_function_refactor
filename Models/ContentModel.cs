﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectRefactor.Models
{
    public class ContentModel
    {
        public Guid Uid { get; set; }

        public string Name { get; set; }

        public Dictionary<string, string> Metadata { get; set; }
    }
}
